function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${starts} to ${ends}
      </div>
       </div>
    `;
  }
  
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Bad response')
      } else {
        const data = await response.json();
        let x = 0
        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const formatter = new Intl.DateTimeFormat('en-US');
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const starts = new Date(details.conference.starts)
            const startsString = formatter.format(starts)
            const ends = new Date(details.conference.ends)
            const endsString = formatter.format(ends)
            const html = createCard(title, description, pictureUrl, startsString, endsString, location);
            const column = document.querySelector(`#column-${x % 3}`);
            x += 1
            column.innerHTML += html;

        } 
    }

      }
    } catch (e) {
      error => console.error('error', error)
    }
  
  });